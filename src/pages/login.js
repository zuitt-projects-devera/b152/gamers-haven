import {useState, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
/*import {useParams, Link} from 'react-router-dom';*/

import Swal from 'sweetalert2';
import UserContext from '../userContext';

export default function Login() {
const {user, setUser} = useContext(UserContext);

const [email, setEmail] = useState("");
const [password, setPassword] = useState("");


 function loginUser(e) {

        e.preventDefault();


        fetch('http://gamers-haven-website.herokuapp.com/users/login',
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"            
            }, 
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
        
            if(data.accessToken) { 
                Swal.fire({

                icon: "success",
                title: "Login Successful",
                text: `Welcome, thank you for logging in.`
            })

            localStorage.setItem('token', data.accessToken)
            let token = localStorage.getItem('token');

            fetch('http://gamers-haven-website.herokuapp.com/users/getUserDetails', {
                method: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`             
                }
            })
            .then(res => res.json())
            .then(data => {

                setUser({
                        id: data._id,
                        isAdmin: data.isAdmin,
                        firstName:data.firstName,
                        lastName:data.lastName,
                        mobileNo: data.mobileNo
                    })
                  
            })
        }
        else {
                Swal.fire({
                icon: "error",
                title: "Login Failed",
                text: data.message
    
            })
        }      
        })
    }

	return(
         user.id
        ?
        <Navigate to="/" replace={true} />
        :

		<div className="col-12 col-md-4 mx-auto mt-5 pt-5">
		<h1 className="text-center">Login</h1> 

		 <Form onSubmit = {e => loginUser(e)}>
            <Form.Group>
                <Form.Label>Email: </Form.Label>             
                <Form.Control className="mb-2" type="text" placeholder="Enter Email" value={email} 
                onChange={e => {setEmail(e.target.value)}}required></Form.Control>
            </Form.Group>
           
            <Form.Group>
                <Form.Label>Password: </Form.Label>
                <Form.Control className="mb-2" type="password" placeholder="Enter Password" value={password} 
                onChange={e => {setPassword(e.target.value)}}required></Form.Control>
            </Form.Group>
              <div className="text-center">
              <Button variant="primary" type="submit" className="my-5">Login</Button>        
              </div>
        </Form>
		</div>





		)
}