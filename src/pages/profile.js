import {useState, useContext, useEffect} from 'react';
import UserContext from '../userContext';
import {Row, Col, Card, Modal, Button} from 'react-bootstrap'

import EditProfile from '../components/editprofile'

export default function Profile() {


    const [showAdd, setShowAdd] = useState(false);//state for hiding/showing add product modal
    const {user, setUser} = useContext(UserContext);
    const handleShowAdd = () => setShowAdd(true);
    const handleCloseAdd = () => setShowAdd(false); 


    const handleShowAlert = () => {
        setShowAdd(true);
        setTimeout(()=> {
            setShowAdd(false);
        }, 2000)
    }

    useEffect(() => {
        handleCloseAdd();

        return () => {
            handleShowAlert();
        }
    }, [])
    return (


        <>
        <Row>

        <Col xs={12} >
       
        <Card className = "p-3 mt-3 cardHighlight">
         <h1 className="text-center">User Profile</h1>
        <Card.Body>
        <Card.Title className="text-center">
        
        </Card.Title>
        <Card.Text className="my-3">
    
        First Name: {user.firstName}

        </Card.Text>
        <Card.Text className="my-3">
         
        LastName: {user.lastName}
        </Card.Text>
        <Card.Text>
   
        Mobile Number: {user.mobileNo}
        </Card.Text>
        <Button variant ="primary" className="my-5" onClick={handleShowAdd}>Update User Information</Button>
        </Card.Body>                   
        </Card>  

        </Col>

        </Row>

        <Modal show={showAdd} onHide={handleCloseAdd}>
       <Modal.Header closeButton>
       
      </Modal.Header>
      <Modal.Body>

         <Card className = "mt-3 bg-light">
                <h1 className="text-center ">Update User Information</h1>
                
                 <Card.Body className="text-dark">
                 
                 <EditProfile />
               
                 </Card.Body>                 

             </Card>
      </Modal.Body>
    
        </Modal>
        </>
        )
}
