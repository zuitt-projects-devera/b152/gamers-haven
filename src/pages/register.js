import {useState, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import {Link} from 'react-router-dom'
import UserContext from '../userContext';

import Swal from 'sweetalert2';


export default function Register() {


 const [firstName, setFirstName] = useState("");
 const [lastName, setLastName] = useState("");
 const [email, setEmail] = useState("");
 const [mobileNo, setMobileNo] = useState("");
 const [password, setPassword] = useState("");
 const [confirmPassword, setConfirmPassword] = useState("");

 const {user, setUser} = useContext(UserContext);
    console.log(password);
    console.log(confirmPassword);
 function registerUser(e) {


    if(confirmPassword !== password) {
            Swal.fire({

                icon: "error",
                title: "Registration Failed",
                text: "Confirm password."

            }) 
    }
    else {
        fetch('http://gamers-haven-website.herokuapp.com/users/',
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"            
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
          
            
               if(data.email) { 
                Swal.fire({

                    icon: "success",
                    title: "Registration Successful",
                    text: "Thank you for registering.",
                    
                })
                window.location.href="/login"
            }
            else {
                Swal.fire({

                    icon: "error",
                    title: "Registration Failed",
                    text: data.message 

                })
            }
       
    })
     
    }
 }

    return (
      user.id
      ?
      <Navigate to ="/products" replace={true} />
      :
      <div className="col-12 col-md-4 mx-auto mt-5  pt-5">
      <h1 className="text-center">Register</h1> 


      <Form  onSubmit = {e => registerUser(e)}>
      <Form.Group>
      <Form.Label>First Name: </Form.Label>             
      <Form.Control className="mb-2" type="text" placeholder="Enter First Name" value={firstName} 
      onChange={e => {setFirstName(e.target.value)}}required></Form.Control>
      </Form.Group>
      <Form.Group>
      <Form.Label>Last Name: </Form.Label>
      <Form.Control className="mb-2" type="text" placeholder="Enter Last Name" value={lastName} 
      onChange={e => {setLastName(e.target.value)}}required></Form.Control>
      </Form.Group>
      <Form.Group>
      <Form.Label>Email Name: </Form.Label>
      <Form.Control className="mb-2" type="text" placeholder="Enter Email" value={email} 
      onChange={e => {setEmail(e.target.value)}} required></Form.Control>
      </Form.Group>
      <Form.Group>
      <Form.Label>Mobile Number: </Form.Label>
      <Form.Control className="mb-2" type="text" placeholder="Enter Mobile Number" value={mobileNo} 
      onChange={e => {setMobileNo(e.target.value)}}required></Form.Control>
      </Form.Group>
      <Form.Group>
      <Form.Label>Password: </Form.Label>
      <Form.Control className="mb-2" type="password" placeholder="Enter Password" value={password} 
      onChange={e => {setPassword(e.target.value)}}required></Form.Control>
      </Form.Group>
      <Form.Group>
      <Form.Label>Confirm Password: </Form.Label>
      <Form.Control className="mb-2" type="password" placeholder="Confirm Password" value={confirmPassword} 
      onChange={e => {setConfirmPassword(e.target.value)}}required></Form.Control>
      </Form.Group>
      <div className="text-center">
      <Button variant="primary" type="submit" className="my-5">Register</Button>
      <p className="text-center text dark">Already have an account? Click <Link to="/login">Here</Link> to Login</p>
      </div>
      
      </Form>
      </div>

      )
}