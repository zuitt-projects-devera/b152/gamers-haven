import {useState, useContext, useEffect} from 'react';
import Banner from '../components/banner';
import UserContext from '../userContext';
import Products from '../components/product'
import AdminDashboard from '../components/admindashboard';


import {Row} from 'react-bootstrap'


export default function ProductsPage() {


const {user} = useContext(UserContext);

   let productsBanner = {
    title: "Gaming Products at your fingertips!",
    description: "Browse our products below",
    buttonText: "Shop Now!",
    destination: "#productsection"
}

const [productsArray, setproductsArray] = useState([]);



//create useEffect which will retrieve our courses on initial render.
useEffect(() => {
    /*fetch all ACTIVE courses
      For requests that are GET method AND does not need to pass token,
      we don't need to add {options}*/
    fetch("http://gamers-haven-website.herokuapp.com/products")
    .then(res => res.json())
    .then(data => {

        setproductsArray(data.map(products => {
            return (
 
                <Products key={products._id} ProductsProp = {products} />


                )
        }));
            
         })
}, [])

    return (
          user.isAdmin 
        ? <AdminDashboard />
        :
        <>

        <Banner  className="text-dark p-5 text-light hero-container" bannerProp={productsBanner}/>
         <section id="productsection">
        <h1 className="text-center my-5">Browse Products</h1>

       <Row xs={1} md={2} className="g-4">
        
        
        {productsArray}

       </Row>
     </section>

     

        </>
        )
    }
    