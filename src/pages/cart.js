import {useState, useContext, useEffect} from 'react';
import UserContext from '../userContext';
import {Row, Col, Card, Modal, Button} from 'react-bootstrap'
import { add, total, list, remove} from 'cart-localstorage'

import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
import EditProfile from '../components/editprofile'

export default function Cart() {

    const [cart, setCart] = useState([]);
    const [totalAmount, setTotalAmount] = useState(0);

    //setCart(list());
    //console.log(cart);
    const cartMap = list();
    const [showAdd, setShowAdd] = useState(false);//state for hiding/showing add product modal
    const {user, setUser} = useContext(UserContext);
    
   // setTotalAmount();
   function removeFromCart(productId) {
    remove(productId);
    window.location.href="/cart"

}

useEffect(() => { 

    setCart (cartMap.map(cartProducts => {  

           /* let totalAmountPerProduct = cartProducts.price * cartProducts.quantity;
            setTotalAmount(totalAmountPerProduct);
            console.log(totalAmountPerProduct);  */            
            return (

                <Card.Body key = {cartProducts.id}>

                <Card.Text className="my-3">
                Name: {cartProducts.name}
                </Card.Text>

                <Card.Text className="my-3">
                Price: {cartProducts.price}
                </Card.Text>

                <Card.Text className="my-3">
                Quantity {cartProducts.quantity}
                </Card.Text>
            {/*<Button className="bg-danger" onClick={()=>{removeFromCart(cartProducts.id)}}>Remove</Button>*/}
            <Link className="btn btn-danger btn-block actions-btn text-right" to="/cart" onClick={()=>{removeFromCart(cartProducts.id)}}>Remove</Link>
            </Card.Body>

            )
        }))
    setTotalAmount(total());
}, [])

    function checkOut() { //archive product
        fetch(`http://gamers-haven-website.herokuapp.com/orders` ,{
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                totalAmount: totalAmount,
                productsPurchased: cart
            })   
        })
        .then(res => res.json())
        .then(data => {
            //console.log(data);
            Swal.fire({
                icon: "success",
                title: "Product Added Successfully",
            })

            window.location.href="/products"
        })
    }  // end of archive product
    return (
        <>
        <Card /*key = {cartProducts.id}*/ className = "p-3 mt-3 cardHighlight">
        <Card.Header>
        <Card.Title className="text-center">Cart</Card.Title>
        </Card.Header>
        <Card.Text className="mt-5">Products:</Card.Text>
        {cart}
        <Card.Text >Total Amount: {totalAmount}</Card.Text>
        {cartMap.length === 0 
            ?
            <Button variant ="success" className="my-5 rounded text-right" disabled>Check Out</Button>
            :
            <Button variant ="success" className="my-5 rounded text-right" /*onClick={()=>{checkOut()}}*/>Check Out</Button>
        }
        </Card>
        </>
        )
}
