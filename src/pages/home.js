import {useContext} from 'react';
import Banner from "../components/banner";
import Services from "../components/services";
import Contact from "../components/contact";
import AdminDashboard from '../components/admindashboard';
import UserContext from '../userContext';

export default function Home() {

const {user} = useContext(UserContext);
 // console.log(user.firstName);
    let bannerData = {
        title: "Gamer's Haven",
        description: "We sell online gaming consoles, accessories and video games. We terminated the hassle of going into physical stores to buy the newly released game. ",
        buttonText: "View what we offer",
        destination: "/products"
    }

    let serviceProp = {
        title: "What do we offer??",
 
    }

    return (

        user.isAdmin 
        ? <AdminDashboard />
        :
        <>

        <Banner bannerProp={bannerData}/>
        <Services servicesProp={serviceProp}/>
        <Contact />
        </>
        )                              
}