import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom'
import {Container} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {UserProvider} from './userContext';


import NavBar from './components/navBar';
import Footer from './components/footer';

import Home from './pages/home';
import ProductsPage from './pages/productspage';
import Login from './pages/login';
import Register from './pages/register';
import Cart from './pages/cart';
import LogOut from './pages/logout';
import Profile from './pages/profile';
import ErrorPage from './pages/errorPage';

import './App.css';

export default function App () {

  const [user, setUser] = useState({
    id: null,
    firstName: null,
    lastName: null,
    mobileNo: null,
    isAdmin: null
  })

useEffect(() => {
    
    fetch('http://gamers-haven-website.herokuapp.com/users/getUserDetails', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }

    })
    .then(res => res.json())
    .then(data => {

      setUser({
        id: data._id,
        firstName: data.firstName,
        lastName: data.lastName,
        mobileNo: data.mobileNo,
        isAdmin: data.isAdmin
      })
    })//2nd then

  }, [])


  const unsetUser = () => {

    localStorage.clear();
  }

  return (
    <>
    <UserProvider value ={{user, setUser, unsetUser}} >
    <Router>

    <NavBar />
     <div className="page-container">
      <div className="content-wrap">
    <Container>

      <Routes>
          <Route path ="/" element={<Home />} />
      <Route path ="/login" element={<Login />} />          
      <Route path ="/products" element={<ProductsPage />} />          
      <Route path ="/register" element={<Register />} />  
      <Route path ="/cart" element={<Cart />} />  
      <Route path ="/profile" element={<Profile />} />  
      <Route path ="/logout" element={<LogOut />} /> 
      <Route path = "*" element ={<ErrorPage />} /> 
  
      </Routes>

    </Container>
     
      </div>
    <Footer />
  </div>
    </Router>
    </UserProvider >
    </>

    )
}