//import react bootstrap components
//react-bootstrap components are react components that create/return react elements with the appropriate bootstrap classes.
import {Row, Col, Card} from 'react-bootstrap'

export default function Services({servicesProp}){
   
 

	return (
  
		<Row className="text-center">
        <h1 className="my-5 text-center">{servicesProp.title}</h1>
			<Col xs={12} md={4}>

                <Card className = "cardHighlight p-3 mt-3">
                 <Card.Img variant="top" src={require('../imgs/console.png')} height="250"/>
                    <Card.Body>
                        <Card.Title>
                            <h2>Gaming Consoles</h2>
                        </Card.Title>
                        <Card.Text>
                            Even if we're old, the kid inside us still wants to play! We sell gaming consoles from 
                            Playstation, Xbox, Nintendo and many more. You may check it in the products page.
                        </Card.Text>
                    </Card.Body>                   
                </Card>                
			</Col>
            <Col xs={12} md={4}>
                <Card className = "cardHighlight p-3  mt-3">
                 <Card.Img variant="top" src={require('../imgs/video-game.png')} height="250"/>
                    <Card.Body>
                        <Card.Title>
                            <h2>Video Games</h2>
                        </Card.Title>
                        <Card.Text>
                            We know playing video games helps you to relax and enjoy your weekend, so we've also 
                            added it into our product lists. Instead of going into physical stores, we will deliver 
                            it in front of your door! You may check the lists of video games we have in the products page.
                        </Card.Text>
                    </Card.Body>                   
                </Card>                
			</Col>
            <Col xs={12} md={4}>
                <Card className = " cardHighlight p-3  mt-3">
                 <Card.Img variant="top" src={require('../imgs/repair.png')} height="250" />
                    <Card.Body>
                        <Card.Title>
                            <h2>Repair Services</h2>
                        </Card.Title>
                        <Card.Text>
                            We accept repair services for your broken gaming console. You may visit our physical
                            store located at ABC Street, Quezon City, Philippines
                        </Card.Text>
                    </Card.Body>                   
                </Card>                
			</Col>
		</Row>

	)

}
