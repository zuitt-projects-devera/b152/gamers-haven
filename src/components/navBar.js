import {useContext} from 'react';
import {Nav,Navbar,Container, NavDropdown} from 'react-bootstrap'
import UserContext from '../userContext';
import {Link} from 'react-router-dom'
import { add, total, list } from 'cart-localstorage'

export default function NavBar() {
	 let cart = {
	 	count: list().length
	 };
	 /*console.log(list());*/
	const {user} = useContext(UserContext);
	return(
		
			<Navbar collapseOnSelect expand="lg" bg="light" variant="light" className=" py-1">
			<Container >
			<Navbar.Brand>
			<Link to="/" className="nav-link text-dark">Gamer's Haven
			{/*<img
		        src={require('../imgs/game-console.png')}
		        width="30"
		        height="30"
		        className="d-inline-block align-top"
		        alt="React Bootstrap logo"
      		/>*/}
      		</Link>
			
			</Navbar.Brand>
			      
				<Navbar.Toggle aria-controls="responsive-navbar-nav"/>
				<Navbar.Collapse id="responsive-navbar-nav" >
					<Nav className="me-auto">
						{user.isAdmin
							? null
							: 
						<>
						<Link to="/" className="text-dark nav-link navText">Home</Link>
						<Link to="/products"  className="text-dark nav-link navText">Gaming Products</Link>
						<Nav.Link href="/#contact"  className="text-dark nav-link navText">Contact Us</Nav.Link>

					
						</>
						}
						{user.id && user.isAdmin === false
						?
						<>
						{ cart.count === 0
							?
							<Link to="/cart"  className="text-dark nav-link navText">Cart (0)</Link>
							:
							<Link to="/cart"  className="text-dark nav-link navText">Cart ({cart.count})</Link>
						}
						</>
						: null
						}
				
				</Nav>
				{
					user.id
					?// if there's user id

					user.isAdmin
					? //if user isAdmin
					<>
					<Nav>
					
					<NavDropdown title="Profile" className="dropdown text-dark nav-link ">
			        <NavDropdown.Item href="/profile" className="text-dark">Profile</NavDropdown.Item>

			        <NavDropdown.Divider />
			        <NavDropdown.Item href="/logout" className="text-dark">Logout</NavDropdown.Item>
			        </NavDropdown>
					</Nav>
					</>
					: //if user isAdmin		
					<Nav>
					<NavDropdown title="Orders" className="text-dark nav-link dropdown">
			         <NavDropdown.Item href="/orders" className="text-dark">Orders</NavDropdown.Item>
			         <NavDropdown.Item href="/profile" className="text-dark">Profile</NavDropdown.Item>

			         <NavDropdown.Divider />
			         <NavDropdown.Item href="/logout" className="text-dark">Logout</NavDropdown.Item>
			        </NavDropdown>
					
					</Nav>
					://if there's user id
					<>
					<Nav>
						
						<Link to="/register"  className="text-dark nav-link ">Register</Link>
						<Link to="/login"  className="text-dark nav-link ">Login</Link>
					</Nav>
					</>	
				}				
				</Navbar.Collapse>
			</Container>
			</Navbar>		
		)
}