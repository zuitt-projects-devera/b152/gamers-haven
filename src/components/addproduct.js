import {useState, useContext} from 'react';
import {Form, Button, Dropdown, DropdownButton} from 'react-bootstrap';


import UserContext from '../userContext';
import Swal from 'sweetalert2';


export default function AddProduct() {


   const [name, setName] = useState("");
   const [cat, setCat] = useState("");
   const [desc, setDesc] = useState("");
   const [price, setPrice] = useState("");
  


   function addProduct(e) { //register function
        //prevent submit event's default behavior
        e.preventDefault();
        Swal.fire({
            title: 'You sure you want to add this product?',
            text: "  ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch('http://gamers-haven-website.herokuapp.com/products/createProduct',
                {
                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json",
                        'Authorization': `Bearer ${localStorage.getItem('token')}`    
                    },
                    body: JSON.stringify({
                        name: name,
                        category: cat,
                        description: desc,
                        price: price
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data.name) { 
                        Swal.fire({
                            icon: "success",
                            title: "Product Added Successfully",
                        })
                        window.location.href="/"
                    }
                    else {
                        Swal.fire({
                            icon: "error",
                            title:"Something went wrong",
                            text: data.message 
                        })
                    }

                })
            }
         })
    } //end of product add

	return (
          /*user.id
        ?
        <Navigate to ="/products" replace={true} />
        :*/
		<div>
		<h1 className="text-center">Add Product</h1> 


        <Form onSubmit = {e => addProduct(e)}>
            <Form.Group>
                <Form.Label>Product Name: </Form.Label>             
                <Form.Control className="mb-2" type="text" placeholder="Enter Product Name" value={name} 
                onChange={e => {setName(e.target.value)}}required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Category: </Form.Label>
               {/* <Form.Control className="mb-2" type="text" placeholder="Enter Category" value={cat} 
                onChange={e => {setCat(e.target.value)}}required></Form.Control>*/}
                <DropdownButton id="dropdown-basic-button" title="Select Category"  >
                  <Dropdown.Item  onClick={e => {setCat("Video Game")}}>Video Game</Dropdown.Item>
                  <Dropdown.Item  onClick={e => {setCat("Gaming Console")}}>Gaming Console</Dropdown.Item>
                  <Dropdown.Item  onClick={e => {setCat("Gaming Peripherals")}}>Gaming Peripherals</Dropdown.Item>
                </DropdownButton>
            </Form.Group>
            <Form.Group>
                <Form.Label >Description: </Form.Label>
                <Form.Control className="mb-2" type="text" placeholder="Enter Description" value={desc} 
               onChange={e => {setDesc(e.target.value)}} required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Price: </Form.Label>
                <Form.Control className="mb-2" type="text" placeholder="Enter Price" value={price} 
                onChange={e => {setPrice(e.target.value)}}required></Form.Control>
            </Form.Group>
            
            <div className="text-center">
            <Button variant="primary" type="submit" className="my-5">Add Product</Button>
            </div>
            
        </Form>
		</div>

		)
}