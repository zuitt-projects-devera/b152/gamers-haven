

export default function Footer() {


	return(

		<>
		<div className= "main-footer bg-dark">
		<div className= "container">
		<div className= "row text-center">
		<div className= "col">
			<h4>Gamer's Haven</h4>	
			<ul className="footer-ul">
				<li><a href="/" className="footer-links">Home</a></li>
				<li><a href="/products" className="footer-links">Gaming Products</a></li>
				<li><a href="/" className="footer-links">FAQs</a></li>
				<li><a href="/" className="footer-links">Contact Us!</a></li>
			</ul>

		</div>	
		<div className= "col ">
			<h4 className="mt-5">Gamer's Haven</h4>	
		

		</div>
		<div className= "col">
			<h4>Follow Us!</h4>	
			<ul className="footer-ul">
				<li><a href="/" className="footer-links">Facebook</a></li>
				<li><a href="/" className="footer-links">Instagarm</a></li>
				<li><a href="/" className="footer-links">Twitter</a></li>
			</ul>
		</div>				
		</div>	
		<div className="row">
			<p className="text-xs-center col-sm text-center">&copy;{new Date().getFullYear()} Gamer's Haven - All Rights Reserved</p>
		</div>	
		</div>		
		</div>

		</>
		
		)



}