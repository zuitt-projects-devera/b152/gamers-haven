import {useState} from 'react';
import {Form, Button, Dropdown, DropdownButton} from 'react-bootstrap';

import Swal from 'sweetalert2';


export default function EditProduct({productsProp}) {

   console.log(productsProp.id)
   const [name, setName] = useState("");
   const [cat, setCat] = useState("");
   const [desc, setDesc] = useState("");
   const [price, setPrice] = useState("");
  


   function editProduct(e) { //register function
        //prevent submit event's default behavior
        e.preventDefault();
        Swal.fire({
            title: 'You sure you want to update this product?',
            text: "  ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(`http://gamers-haven-website.herokuapp.com/products/editProduct/${productsProp.id}`,
                {
                    method: 'PUT',
                    headers: {
                        "Content-Type": "application/json",
                        'Authorization': `Bearer ${localStorage.getItem('token')}`    
                    },
                    body: JSON.stringify({
                        name: name,
                        category: cat,
                        description: desc,
                        price: price
                    })
                })
                .then(res => res.json())
                .then(data => {

                    if(data.name) { 
                        Swal.fire({
                            icon: "success",
                            title: "Product Updated Successfully",
                        })
                        
                        window.location.href="/"
                    }
                    else {
                        Swal.fire({

                            icon: "error",
                            title:"Something went wrong",
                            text: data.message 
                        })
                    }

                })
            }
         })
    } //end of edit product

    return (
          /*user.id
        ?
        <Navigate to ="/products" replace={true} />
        :*/
        <div>
        <h1 className="text-center">Update Product Information</h1> 


        <Form onSubmit = {e => editProduct(e)}>
            <Form.Group>
                <Form.Label>Product Name: </Form.Label>             
                <Form.Control className="mb-2" type="text" placeholder="Enter Product Name" value={name} 
                onChange={e => {setName(e.target.value)}}></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Category: </Form.Label>
              
                <DropdownButton id="dropdown-basic-button" title="Select Category"  >
                  <Dropdown.Item  onClick={e => {setCat("Video Game")}}>Video Game</Dropdown.Item>
                  <Dropdown.Item  onClick={e => {setCat("Gaming Console")}}>Gaming Console</Dropdown.Item>
                  <Dropdown.Item  onClick={e => {setCat("Gaming Peripherals")}}>Gaming Peripherals</Dropdown.Item>
                </DropdownButton>
            </Form.Group>
            <Form.Group>
                <Form.Label >Description: </Form.Label>
                <Form.Control className="mb-2" type="text" placeholder="Enter Description" value={desc} 
               onChange={e => {setDesc(e.target.value)}} ></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Price: </Form.Label>
                <Form.Control className="mb-2" type="text" placeholder="Enter Price" value={price} 
                onChange={e => {setPrice(e.target.value)}}></Form.Control>
            </Form.Group>
            
            <div className="text-center">
            <Button variant="primary" type="submit" className="my-5">Update Product</Button>
            </div>
            
        </Form>
        </div>

        )
}