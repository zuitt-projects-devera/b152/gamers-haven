import {useState, useContext} from 'react';
import {Form, Button, Dropdown, DropdownButton} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import {Link} from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';


export default function EditProfile() {

   
   const [firstName, setFirstName] = useState("");
   const [lastName, setLastName] = useState("");
   const [mobileNo, setMobileNo] = useState("");

    const {user, setUser} = useContext(UserContext);   
  /*

   const {user, setUser} = useContext(UserContext);
*/

   function editProfile(e) { //update function
        //prevent submit event's default behavior
      
        e.preventDefault();
        Swal.fire({
            title: 'You sure you want to update your user information?',
            text: "  ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(`http://gamers-haven-website.herokuapp.com/users/editUserDetails`,
                {
                    method: 'PUT',
                    headers: {
                        "Content-Type": "application/json",
                        'Authorization': `Bearer ${localStorage.getItem('token')}`    
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data => {

                    if(data._id) { 
                        Swal.fire({
                            icon: "success",
                            title: "User Updated Successfully",
                        })
                        
                        window.location.href="/profile"
                    }
                    else {
                        Swal.fire({

                            icon: "error",
                            title:"Something went wrong",
                            text: data.message 
                        })
                    }
                     
                })
            }
         })

    } //end of edit profile

    return (
      


        <div>
        


        <Form onSubmit = {e => editProfile(e)}>
           <Form.Group>
                <Form.Label>First Name: </Form.Label>             
                <Form.Control className="mb-2" type="text" placeholder="Enter New First Name" value={firstName} 
                onChange={e => {setFirstName(e.target.value)}}></Form.Control>
            </Form.Group>
           
            <Form.Group>
                <Form.Label >Last Name: </Form.Label>
                <Form.Control className="mb-2" type="text" placeholder="Enter New Last Name" value={lastName} 
               onChange={e => {setLastName(e.target.value)}} ></Form.Control>
            </Form.Group>
              <Form.Group>
                <Form.Label>Mobile Number: </Form.Label>
                <Form.Control className="mb-2" type="text" placeholder="Enter New Mobile Number" value={mobileNo} 
                onChange={e => {setMobileNo(e.target.value)}}></Form.Control>
            </Form.Group>
            
            <div className="text-center">
            <Button variant ="primary" type="submit" className="mb-5"> Save</Button>
            </div>
            
        </Form>
        </div>
        )
}