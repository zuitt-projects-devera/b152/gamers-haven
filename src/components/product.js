import {Col, Card, Modal, Dropdown, DropdownButton} from 'react-bootstrap'
import {useState, useContext, useEffect} from 'react';
import UserContext from '../userContext';
import {Link} from 'react-router-dom';

import { add, total } from 'cart-localstorage'


export default function Products({ProductsProp}){

const [showAdd, setShowAdd] = useState(false);//state for hiding/showing add product modal
const [quantity, setQuantity] = useState(1);//
const {user, setUser} = useContext(UserContext);


const handleShowAdd = () => 
setShowAdd(true);

const handleCloseAdd = () => setShowAdd(false); 


const handleShowAlert = () => {
    setShowAdd(true);
    setTimeout(()=> {
        setShowAdd(false);
    }, 2000)
}
useEffect(() => {
    handleCloseAdd();
    return () => {
        handleShowAlert();
    }
}, [])

function addToCart() {
    add({id: ProductsProp._id, name: ProductsProp.name, price: ProductsProp.price}, quantity) 

    handleCloseAdd();
    window.location.href="/products#productsection";
   // console.log(quantity);
}
/*${ProductsProp._id}*/
return (

    <>

    <Col xs={12} md={4} >

    <Card className = "cardHighlight mt-3 bg-light">
    <Card.Img variant="top" src={require('../imgs/game-console.png')} height="250px"  />
    <Card.Body className=" bg-dark text-light">
    <Card.Title className="text-center">{ProductsProp.name}</Card.Title>
    <Card.Text className="mb-5">{ProductsProp.description}</Card.Text>
    <Link to={`/products/`} className="btn btn-primary text-bottom product-btn" onClick={handleShowAdd}>View Product</Link>

    </Card.Body>                 

    </Card>  

    </Col>



    <Modal show={showAdd} onHide={handleCloseAdd}>
    <Modal.Header closeButton>
    <Modal.Title id="contained-modal-title-vcenter">

    </Modal.Title>
    </Modal.Header>
    <Modal.Body>

    <Card className = "mt-3 bg-light">
    <h1 className="text-center">{ProductsProp.name}</h1>
    <Card.Img variant="top" src={require('../imgs/game-console.png')} height="250px"  />
    <Card.Body className="text-dark text-center">

    <Card.Text className="mb-3">{ProductsProp.description}</Card.Text>
    <h3 className="text-center">Price: ₱{ProductsProp.price}.00</h3>

    {
        user.id 
        ?
        <>


         <DropdownButton id="dropdown-basic-button" className="my-3 " title="Select Quantity"  >
                  <Dropdown.Item  onClick={e => {setQuantity(1)}}>1</Dropdown.Item>
                  <Dropdown.Item  onClick={e => {setQuantity(2)}}>2</Dropdown.Item>
                  <Dropdown.Item  onClick={e => {setQuantity(3)}}>3</Dropdown.Item>
                </DropdownButton>
        
        <Link to="/products" className="btn btn-success text-bottom " onClick={()=>{addToCart()}}>Add To Cart</Link>          
        </>
        :
        <Link to='/login' className="btn btn-primary text-bottom" onClick={handleShowAdd}>Login To Proceed</Link>
    }
    </Card.Body>                 

    </Card>
    </Modal.Body>
    
    </Modal>
    </>



    )

}
