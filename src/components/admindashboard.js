import {useState, useEffect, useContext} from 'react';
import UserContext from '../userContext';
import AddProduct from './addproduct';
import EditProduct from './editproduct';
import {Table, Button, Modal} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import {useParams, Link} from 'react-router-dom';

export default function AdminDashboard() {

	const {user} = useContext(UserContext);
	const [product_id, setProduct_Id] = useState("");

	let prodProp = {
		id: product_id
	}
	const [allProducts, setAllProducts] = useState([]);
	//archive function

 	const [showAdd, setShowAdd] = useState(false);//state for hiding/showing add product modal
 	const [showEdit, setShowEdit] = useState(false);//state for hiding/showing add product modal
    
    const handleShowAdd = () => setShowAdd(true);
    const handleCloseAdd = () => setShowAdd(false); 

    const handleShowEdit = () => 
    setShowEdit(true) 



    const handleCloseEdit = () => 
    setShowEdit(false) 
  

    const handleEditShowAlert = () => {
        setShowEdit(true);
        setTimeout(()=> {
            setShowEdit(false);
        }, 2000)
    }

     useEffect(() => {
        handleCloseAdd();

        return () => {
            handleShowAlert();
        }
    }, [])

      const handleShowAlert = () => {
        setShowAdd(true);
        setTimeout(()=> {
            setShowAdd(false);
        }, 2000)
    }

     useEffect(() => {
        handleCloseEdit();

        return () => {
            handleEditShowAlert();
        }
    }, [])



	useEffect(() => {
		if(user.isAdmin){
			//fetch all courses
			fetch('http://gamers-haven-website.herokuapp.com/products/getAllProducts',
				{
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setAllProducts (data.map(products => {					
					return (
							<tr key = {products._id}>
								<td>{products._id}</td>
								<td>{products.name}</td>
								<td>{products.price}</td>
								<td>{products.isActive ? "Active": "Inactive"}</td>
								<td className="text-center">
								{
								products.isActive 

								?<><Button variant ="danger" className="mx-2 actions-btn" onClick={()=>{archive(products._id)}}>Disable</Button>
								   {/*<Link className="btn btn-primary btn-block actions-btn" to="./UpdateProduct" productProps={productProp}  onClick={handleShowEdit}>Update</Link>*/}
								     <Button variant ="primary" className="btn btn-primary btn-block actions-btn" onClick={()=>{getProductId(products._id)}}>Update</Button>
								 </>
								:<><Button variant ="success" className="mx-2 actions-btn" onClick={()=>{activate(products._id)}}>Enable</Button>
								  {/*<Link className="btn btn-primary btn-block actions-btn" productProps={productProp}  >Update</Link>*/}
								  <Button variant ="primary" className="btn btn-primary btn-block actions-btn" onClick={()=>{getProductId(products._id)}}>Update</Button>
								 </>
								}
								</td>
								
							</tr>
						)
				}))
			}) 
		}
	}, [])


		function archive(productId) { //archive product
		fetch(`http://gamers-haven-website.herokuapp.com/products/archiveProduct/${productId}` ,{
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}	
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);
			window.location.href="/"
		})
	}  // end of archive product


	function activate(productId) { 	//activate product

		fetch(`http://gamers-haven-website.herokuapp.com/products/activateProduct/${productId}` ,{
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}	
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);
			window.location.href="/"
		})
	} // end of activate product

	 function getProductId(productId) {
        	handleShowEdit();
        	fetch(`https://gamers-haven-website.herokuapp.com/products/${productId}` ,{
			
			
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);
			setProduct_Id(productId);
        	
		
		})
        	//console.log(product_id);
        }
	return (

		<>
		<h1 className="my-5 text center">Admin Dashboard</h1> 
		<Button variant ="primary" className="mb-5" onClick={handleShowAdd}>Add Product</Button>

		<Table striped bordered hover variant="dark">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Price</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
					{allProducts}
			</tbody>
		</Table>


		 <Modal show={showAdd} onHide={handleCloseAdd}>
        <Modal.Header  closeButton>
           
        </Modal.Header>
        <Modal.Body >
            <AddProduct />
        </Modal.Body>
    
    	</Modal>



    	<Modal show={showEdit} onHide={handleCloseEdit}>
        <Modal.Header  closeButton>
           
        </Modal.Header>
        <Modal.Body >
            <EditProduct productsProp = {prodProp}/>
        </Modal.Body>

    	</Modal>
		</>

		)
}