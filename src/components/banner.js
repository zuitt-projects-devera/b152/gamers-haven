
import {Row, Col} from 'react-bootstrap'


export default function Banner({bannerProp}){

	return (

		
		<Row>
			<Col className="p-5 text-light hero-container">
			
				<h1 className="mb-3">{bannerProp.title}</h1>
				<p className="my-3">{bannerProp.description}</p>
				<a href = {bannerProp.destination} className = "btn btn-primary">{bannerProp.buttonText}</a>
	
			</Col>
		</Row>









		

	)

}
